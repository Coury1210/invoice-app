import Vue from 'vue'
import CreateProducts from '@/components/CreateProducts'

describe('CreateProducts.vue', () => {
  describe('CreateProducts.vue', () => {
    it('renders with mount and does initialize API call', () => {
      const wrapper = mount(CreateProducts)

      expect(wrapper.find(CreateProducts).exists()).toBe(true)
    })
  })
})
