import Vue from 'vue'
import ProductsIndex from '@/components/ProductsIndex'
import {
  shallowMount,
  mount
} from '@vue/test-utils'

describe('ProductsIndex.vue', () => {
  it('renders with mount and does initialize API call', () => {
    const wrapper = mount(ProductsIndex)

    expect(wrapper.find(ProductsIndex).exists()).toBe(true)
  })
})


describe("ProductsIndex", () => {
  it("emits an event with two arguments", () => {
    const wrapper = shallowMount(ProductsIndex)

    wrapper.vm.emitEvent()

    console.log(wrapper.emitted())
  })
})
