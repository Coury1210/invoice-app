import CreateCustomers from '@/components/CreateCustomers'
import {
  shallowMount,
  mount
} from '@vue/test-utils'

describe('CreateCustomers.vue', () => {
  it('renders with mount and does initialize API call', () => {
    const wrapper = mount(CreateCustomers)

    expect(wrapper.find(CreateCustomers).exists()).toBe(true)
  })
})
