import Vue from 'vue'
import {
  shallowMount
} from "@vue/test-utils"
import routes from "@/routes.js"
const localVue = createLocalVue()
localVue.use(VueRouter)
import CustomersIndex from '@/components/CustomersIndex'

describe('CustomersIndex.vue', () => {
  it('should return data', () => {
    const wrapper = shallowMount(CustomersIndex)
    wrapper.find("[data-username]").setValue("alice")
    wrapper.find("form").trigger("submit.prevent")
    expect(wrapper.find(".empty-table").text())
      .toBe("No Customers to display. Check your network connection")
  })
})
describe("App", () => {
  it("renders a child component via routing", () => {
    const router = new VueRouter({
      routes
    })
    const wrapper = mount(App, {
      localVue,
      router
    })

    router.push("/editCustomers/:id")

    expect(wrapper.find(editCustomers).exists()).toBe(true)
  })
})
