import Vue from 'vue'
import EditProducts from '@/components/EditProducts'
import {
  shallowMount
} from "@vue/test-utils"
//API initialization test returns true if connection succeeds
it('renders with mount and does initialize API call', () => {
  const wrapper = mount(EditProducts, {
    stubs: {
      EditProducts: true
    }
  })

  expect(wrapper.find(EditProducts).exists()).toBe(true)
})
