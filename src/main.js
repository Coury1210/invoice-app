import Vue from 'vue'
import App from './App.vue'
import store from './store'
import 'bootstrap/dist/css/bootstrap.min.css'

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';
import Axios from 'axios';

Vue.use(VueAxios, axios);



const token = localStorage.getItem('token')
if (token) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = token
}


Vue.config.productionTip = false;

import RegisterComponent from './components/RegisterComponent.vue';
import LoginComponent from './components/LoginComponent.vue';
import Secure from './components/Secure.vue'

import HomeComponent from './components/HomeComponent.vue';
import CreateComponent from './components/CreateComponent.vue';
import CreateCustomers from './components/CreateCustomers.vue';
import CreateProducts from './components/CreateProducts.vue';
import CreateInvoices from './components/CreateInvoices.vue';

import IndexComponent from './components/IndexComponent.vue';
import CustomersIndex from './components/CustomersIndex.vue';
import EditComponent from './components/EditComponent.vue';
import EditCustomers from './components/EditCustomers.vue';
import EditProducts from './components/EditProducts.vue';
import EditInvoices from './components/EditInvoices.vue';

import InvoiceIndex from './components/InvoiceIndex.vue';
import ProductsIndex from './components/ProductsIndex.vue';
import deleteResult from './components/deleteResult.vue';


const routes = [{
    name: 'home',
    path: '/',
    component: HomeComponent
  },
  {
    name: 'register',
    path: '/register',
    component: RegisterComponent
  },
  {
    name: 'login',
    path: '/login',
    component: LoginComponent
  },
  {
    name: 'deleteResult',
    path: '/deleteResult',
    component: deleteResult
  },
  {
    name: 'create',
    path: '/create',
    component: CreateComponent
  },
  {
    name: 'createProducts',
    path: '/createProducts',
    component: CreateProducts
  }, {
    name: 'createCustomers',
    path: '/createCustomers',
    component: CreateCustomers
  }, {
    name: 'createInvoices',
    path: '/createInvoices',
    component: CreateInvoices
  },
  {
    name: 'products',
    path: '/products',
    component: ProductsIndex
  }, {
    name: 'customers',
    path: '/customers',
    component: CustomersIndex
  }, {
    name: 'invoices',
    path: '/invoices',
    component: InvoiceIndex
  },
  {
    path: '/secure',
    name: 'secure',
    component: Secure,
    meta: {
      requiresAuth: true
    }
  },

  {
    name: 'posts',
    path: '/posts',
    component: IndexComponent
  },
  {
    name: 'edit',
    path: '/edit/:id',
    component: EditComponent
  },
  {
    name: 'editProducts',
    path: '/editProducts/:id',
    component: EditProducts
  },
  {
    name: 'editCustomers',
    path: '/editCustomers/:id',
    component: EditCustomers
  },
  {
    name: 'editInvoices',
    path: '/editInvoices/:id',
    component: EditInvoices
  }
];

const router = new VueRouter({
  mode: 'history',
  routes: routes
});

new Vue(Vue.util.extend({
  router
}, App)).$mount('#app');

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      next()
      return
    }
    next('/login')
  } else {
    next()
  }
})
